﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Data;

using MySql.Data.MySqlClient;

namespace AsprovaDB
{
    public class Database
    {

        #region Used Strings
        string insertQuery117 = "INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}')";
        string insertQuery118 = "INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', {22}, '{23}')";
        string order_due_date = "order_due_date";
        string asprova_max_size = "asprova_max_size";
        string order_code = "order_code";
        string qty = "qty";
        readonly string sales_order_code = "sales_order_code";
        #endregion


        private readonly string connectionString = GetConfig<Config>("common.json").ConnectionString;

        public System.Data.DataTable GetAlldataFromTable(string tableSLug, string query)
        {
            System.Data.DataTable DT = new System.Data.DataTable
            {
                TableName = tableSLug
            };

            MySqlConnection sqlConnection = new MySqlConnection(connectionString);

            string myQuery = query + tableSLug;

            try
            {
                using (MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(myQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    mySqlDataAdapter.Fill(DT);
                }
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
            return DT;
        }
        
        public void UpdateviewTable(DataTable tabletoProcess, string finalTable)
        {
            DataTable comerioExpSource = new DataTable();
            DataTable szeleteloOrderSource = new DataTable();
            DataTable comerioOrderSource = new DataTable();
            comerioExpSource = GetAlldataFromTable("for_view_comerioexport", "SELECT * FROM ");
            szeleteloOrderSource = GetAlldataFromTable("aspexp_order_szel", "SELECT * FROM ");
            comerioOrderSource = GetAlldataFromTable("aspexp_order_comerio", "SELECT * FROM ");

            // Add asp_order table az a source to find order' customers
            DataTable aspOrderSource = GetAlldataFromTable("asp_order", "SELECT * FROM ");
            aspOrderSource.PrimaryKey = new DataColumn[] { aspOrderSource.Columns[0] }; ;

            // Truncate the result table
            TruncateTargetTable(finalTable);

            foreach (DataRow actualRow in tabletoProcess.Rows)
            {
                string connectedOrder;
                string jumboQty;
                string jumboLength;
                string jumboWeight;
                string jumboSize;
                DateTime orderDueDate = new DateTime();
                if (actualRow[order_due_date] != DBNull.Value)
                {
                    orderDueDate = Convert.ToDateTime(actualRow[order_due_date]);
                }
                string asprovaMaxSize = actualRow[asprova_max_size].ToString();

                // Get data from actual table row
                if (tabletoProcess.ToString() == "aspexp_order_comerio")
                {
                    connectedOrder = actualRow["kesz_order"].ToString();
                    jumboQty = actualRow["jumbo_qty"].ToString();
                    jumboLength = actualRow["jumbo_length"].ToString();
                    jumboWeight = actualRow["jumbo_weight"].ToString();
                    jumboSize = actualRow["jumbo_size"].ToString();                
                }
                else
                {
                    connectedOrder = actualRow["felkesz_order"].ToString();
                    jumboWeight = actualRow["jumbo_weight"].ToString();
                    GetJumboSizeFromComerioExp(actualRow["order_code"].ToString(), connectedOrder, comerioExpSource, out jumboQty, out jumboLength, out jumboSize);
                }
                // Generate an order from the actual data row
                ViewOrder actualOrder = new ViewOrder(actualRow["order_code"].ToString(), connectedOrder,
                    actualRow["item_code"].ToString(), actualRow["item_name"].ToString(), actualRow["porkeverek"].ToString(),
                    actualRow["folia_tipus"].ToString(), actualRow["folia_tipus_hosszu"].ToString(), actualRow["color"].ToString(),
                    Convert.ToDouble(actualRow["qty"]), jumboQty, jumboLength, jumboWeight, jumboSize, actualRow["customer"].ToString(),
                    Convert.ToDateTime(actualRow["start_time"]), Convert.ToDateTime(actualRow["end_time"]), actualRow["machine"].ToString(),
                    orderDueDate, asprovaMaxSize, actualRow[sales_order_code].ToString());
                if (tabletoProcess.ToString() == "aspexp_order_comerio")
                {                   
                    actualOrder.adalek = actualRow["adalek"].ToString();
                }

                string query;
                string connectedOrderCustomer = "";
                string MaxRollSizeToDB = "";

                if (actualOrder.ConnectingOrder.Contains(";"))
                {
                    // Store the multiple data (divided by semicolon) in list
                    List<string> separatedConnected = new List<string>();
                    List<string> separatedJumboQty = new List<string>();
                    List<string> separatedJumboLength = new List<string>();
                    List<string> separatedJumboWeight = new List<string>();
                    List<string> separatedJumboSize = new List<string>();

                    // Split the multiple data
                    separatedConnected = actualOrder.ConnectingOrder.Split(';').ToList();
                    separatedJumboQty = actualOrder.JumboQty.Split(';').ToList();
                    separatedJumboLength = actualOrder.JumboLength.Split(';').ToList();
                    separatedJumboWeight = actualOrder.JumboWeight.Split(';').ToList();
                    separatedJumboSize = actualOrder.JumboSize.Split(';').ToList();
                    for (int i = 0; i < separatedConnected.Count(); i++)
                    {
                        string localConnectingOrder;
                        string localjumboQty;
                        string localjumboLength;
                        string localjumboWeight;
                        string localjumboSize;
                        string localConnAsMaxSize;
                        DateTime actualLocalMyDueDate = new DateTime();
                        double actualOrderedQty = 0;
                        string actualSalesCode = "";

                        if (finalTable == "for_view_comerioexport")
                        {
                            localConnectingOrder = separatedConnected[i];
                            localjumboQty = separatedJumboQty[i];
                            localjumboLength = separatedJumboLength[i];
                            localjumboWeight = separatedJumboWeight[i];
                            localjumboSize = separatedJumboSize[i];
                            connectedOrderCustomer = aspOrderSource.Rows.Find(localConnectingOrder)["customer_name"].ToString();
                            MaxRollSizeToDB = GetMaxRollSizeFromOrder(localConnectingOrder, szeleteloOrderSource);
                            localConnAsMaxSize = GetAsRollSizeFromOrder(localConnectingOrder, szeleteloOrderSource);
                            actualLocalMyDueDate = GetDueDateFromOrder(localConnectingOrder, szeleteloOrderSource);
                            actualOrderedQty = GetConnectingQty(localConnectingOrder, szeleteloOrderSource);
                            actualSalesCode = GetConnectingSalesCode(localConnectingOrder, szeleteloOrderSource);
                        }
                        else
                        {
                            localConnectingOrder = "";
                            localjumboQty = "";
                            localjumboLength = "";
                            localjumboWeight = "";
                            localjumboSize = "";
                            connectedOrderCustomer = aspOrderSource.Rows.Find(actualOrder.Code)["customer_name"].ToString();
                            localConnAsMaxSize = aspOrderSource.Rows.Find(actualOrder.Code)[asprova_max_size].ToString();
                            actualLocalMyDueDate = Convert.ToDateTime(aspOrderSource.Rows.Find(actualOrder.Code)[order_due_date]);
                            actualOrderedQty = Convert.ToDouble(aspOrderSource.Rows.Find(actualOrder.Code)[qty]);
                            actualSalesCode = aspOrderSource.Rows.Find(actualOrder.Code)[sales_order_code].ToString();
                        }

                        if (finalTable == "for_view_comerioexport")
                        {
                            query = string.Format(insertQuery118,
                            finalTable, actualOrder.Code, localConnectingOrder, actualOrder.ItemCode, actualOrder.ItemName, actualOrder.Porkeverek,
                            actualOrder.FoliaType, actualOrder.FoliaTypeCatalog, actualOrder.Color, actualOrder.Qty, localjumboQty, localjumboLength,
                            localjumboWeight, localjumboSize, connectedOrderCustomer, actualOrder.StartTime, actualOrder.EndTime, actualOrder.Machine,
                            actualOrder.adalek, MaxRollSizeToDB, actualLocalMyDueDate, localConnAsMaxSize, actualOrderedQty, actualSalesCode);

                            ExecuteCommand(query);
                        }
                        else if (i == separatedConnected.Count() - 1)
                        {
                            query = string.Format(insertQuery117,
                            finalTable, actualOrder.Code, actualOrder.ConnectingOrder, actualOrder.ItemCode, actualOrder.ItemName, actualOrder.Porkeverek,
                            actualOrder.FoliaType, actualOrder.FoliaTypeCatalog, actualOrder.Color, actualOrder.Qty, localjumboQty, localjumboLength,
                            localjumboWeight, localjumboSize, connectedOrderCustomer, actualOrder.StartTime, actualOrder.EndTime, actualOrder.Machine,
                            actualLocalMyDueDate, actualOrder.AsprovaRollMaxSize, actualSalesCode);

                            ExecuteCommand(query);
                        }
                    }
                }
                else if(finalTable != "for_view_comerioexport")
                {
                    connectedOrderCustomer = aspOrderSource.Rows.Find(actualOrder.Code)["customer_name"].ToString();

                    query = string.Format(insertQuery117,
                        finalTable, actualOrder.Code, actualOrder.ConnectingOrder, actualOrder.ItemCode, actualOrder.ItemName, actualOrder.Porkeverek,
                        actualOrder.FoliaType, actualOrder.FoliaTypeCatalog, actualOrder.Color, actualOrder.Qty, actualOrder.JumboQty, actualOrder.JumboLength,
                        actualOrder.JumboWeight, actualOrder.JumboSize, connectedOrderCustomer, actualOrder.StartTime, actualOrder.EndTime, actualOrder.Machine,
                        actualOrder.OrderDueDate, actualOrder.AsprovaRollMaxSize, actualOrder.SalesOrder);

                    ExecuteCommand(query);
                }
                else
                {
                    string localConnAsMaxSize = "";
                    DateTime actualMyDueDate = new DateTime();
                    double actualOrderedQty = 0;
                    string actualSalesCode = "";

                    if (actualOrder.ConnectingOrder != null && actualOrder.ConnectingOrder != "")
                    {
                        connectedOrderCustomer = aspOrderSource.Rows.Find(actualOrder.ConnectingOrder)["customer_name"].ToString();
                        MaxRollSizeToDB = GetMaxRollSizeFromOrder(actualOrder.ConnectingOrder, szeleteloOrderSource);
                        localConnAsMaxSize = GetAsRollSizeFromOrder(actualOrder.ConnectingOrder, szeleteloOrderSource);
                        actualMyDueDate = GetDueDateFromOrder(actualOrder.ConnectingOrder, szeleteloOrderSource);
                        actualOrderedQty = GetConnectingQty(actualOrder.ConnectingOrder, szeleteloOrderSource);
                        actualSalesCode = GetConnectingSalesCode(actualOrder.ConnectingOrder, szeleteloOrderSource);
                    }
                    else
                    {
                        connectedOrderCustomer = aspOrderSource.Rows.Find(actualOrder.Code)["customer_name"].ToString();
                        MaxRollSizeToDB = GetMaxRollSizeFromOrder(actualOrder.Code, comerioOrderSource);
                        localConnAsMaxSize = actualOrder.AsprovaRollMaxSize;
                        actualMyDueDate = actualOrder.OrderDueDate;
                        actualOrderedQty = actualOrder.Qty;
                        if (actualOrder.SalesOrder != null && actualOrder.SalesOrder != "")
                        {
                            actualSalesCode = actualOrder.SalesOrder;
                        }
                    }                    

                    query = string.Format(insertQuery118,
                    finalTable, actualOrder.Code, actualOrder.ConnectingOrder, actualOrder.ItemCode, actualOrder.ItemName, actualOrder.Porkeverek,
                    actualOrder.FoliaType, actualOrder.FoliaTypeCatalog, actualOrder.Color, actualOrder.Qty, actualOrder.JumboQty, actualOrder.JumboLength,
                    actualOrder.JumboWeight, actualOrder.JumboSize, connectedOrderCustomer, actualOrder.StartTime, actualOrder.EndTime, actualOrder.Machine,
                    actualOrder.adalek, MaxRollSizeToDB, actualMyDueDate, localConnAsMaxSize, actualOrderedQty, actualSalesCode);

                    ExecuteCommand(query);
                }
            }
        }

        /// <summary>
        /// Az Asprovából exportált táblából kiszedi, hogy mi volt a használt max roll méret
        /// </summary>
        /// <param name="localConnectingOrder"></param>
        /// <param name="szeleteloOrderSource"></param>
        /// <returns></returns>
        private string GetMaxRollSizeFromOrder(string localConnectingOrder, DataTable expOrderSource)
        {
            string foundRollMax = "";

            foreach (DataRow actualRow in expOrderSource.Rows)
            {
                if (actualRow["order_code"].ToString() == localConnectingOrder)
                {
                    foundRollMax = actualRow["used_max_size"].ToString();
                }
            }

            return foundRollMax;
        }

        private double GetConnectingQty(string localConnectingOrder, DataTable expOrderSource)
        {
            double foundOrderQty = 0;

            foreach (DataRow actualRow in expOrderSource.Rows)
            {
                if (actualRow[order_code].ToString() == localConnectingOrder)
                {
                    foundOrderQty = Convert.ToDouble(actualRow[qty]);
                }
            }
            return foundOrderQty;
        }

        private DateTime GetDueDateFromOrder(string localConnectingOrder, DataTable expOrderSource)
        {
            DateTime foundDueDate = new DateTime();

            foreach (DataRow actualRow in expOrderSource.Rows)
            {
                if (actualRow["order_code"].ToString() == localConnectingOrder)
                {
                    foundDueDate = Convert.ToDateTime(actualRow[order_due_date]);
                }
            }

            return foundDueDate;
        }

        private string GetConnectingSalesCode(string localConnectingOrder, DataTable expOrderSource)
        {
            string foundSalesCode = "";

            foreach (DataRow actualRow in expOrderSource.Rows)
            {
                if (actualRow[order_code].ToString() == localConnectingOrder)
                {
                    foundSalesCode = actualRow[sales_order_code].ToString();
                }
            }
            return foundSalesCode;
        }

        private string GetAsRollSizeFromOrder(string localConnectingOrder, DataTable expOrderSource)
        {
            string foundAsRollSize = "";

            foreach (DataRow actualRow in expOrderSource.Rows)
            {
                if (actualRow[order_code].ToString() == localConnectingOrder)
                {
                    foundAsRollSize = actualRow[asprova_max_size].ToString();
                }
            }

            return foundAsRollSize;
        }

        public void ExecuteCommand(string query)
        {
            MySqlConnection sqlConnection = new MySqlConnection(connectionString);
            try
            {
                using (MySqlCommand command = new MySqlCommand(query, sqlConnection))
                {
                    sqlConnection.Open();
                    command.ExecuteReader();
                }
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public void TruncateTargetTable(string targetTable)
        {
            MySqlConnection myconnection = new MySqlConnection(connectionString);
            try
            {
                String query = string.Format("TRUNCATE TABLE {0}",targetTable);
                using (MySqlCommand command = new MySqlCommand(query, myconnection))
                {
                    myconnection.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                myconnection.Close();
            }
        }

        public static T GetConfig<T>(string configName)
        {
            var cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/config", configName), UTF8Encoding.UTF8);
            return JsonConvert.DeserializeObject<T>(String.Join("\r\n", cfgLines));
        }

        public void GetJumboSizeFromComerioExp(string finalOrder, string intermOrder, DataTable comerioTable, out string jumboQty, out string jumboLength, out string jumboSize)
        {
            jumboQty = "";
            jumboLength = "";
            jumboSize = "";

            bool foundOrder = false;

            foreach (DataRow actualRow in comerioTable.Rows)
            {
                if (actualRow["order_code"].ToString() == intermOrder && actualRow["kesz_order"].ToString() == finalOrder)
                {
                    jumboQty = actualRow["jumbo_qty"].ToString();
                    jumboLength = actualRow["jumbo_length"].ToString();
                    jumboSize = actualRow["jumbo_size"].ToString();
                    foundOrder = true;
                    break;
                }
            }
            if (!foundOrder)
            { }
        }
    }
    public class Config
    {
        public string ConnectionString { get; set; }
    }
}
