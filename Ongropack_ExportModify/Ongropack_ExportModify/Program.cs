﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsprovaDB;
using System.Data;

namespace Ongropack_ExportModify
{
    class Program
    {
        static void Main(string[] args)
        {
            Database OngropackMyExport = new Database();

            DataTable dtComerio = new DataTable();
            DataTable dtSzeletelo = new DataTable();

            dtComerio = OngropackMyExport.GetAlldataFromTable("aspexp_order_comerio", "SELECT * FROM ");
            dtSzeletelo = OngropackMyExport.GetAlldataFromTable("aspexp_order_szel", "SELECT * FROM ");

            Console.WriteLine("Updating Comerio export");
            OngropackMyExport.UpdateviewTable(dtComerio, "for_view_comerioexport");

            Console.WriteLine("Updating Szeletelo export");
            OngropackMyExport.UpdateviewTable(dtSzeletelo, "for_view_szeleteloexport");

        }
    }
}
