﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsprovaDB
{
    public class ViewOrder
    {
        public string Code { get; set; }
        public string ConnectingOrder { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Porkeverek { get; set; }
        public string FoliaType { get; set; }
        public string FoliaTypeCatalog { get; set; }
        public string Color { get; set; }
        public double Qty { get; set; }
        public string JumboQty { get; set; }
        public string JumboLength { get; set; }
        public string JumboWeight { get; set; }
        public string JumboSize { get; set; }
        public string Customer { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime OrderDueDate { get; set; }
        public string Machine { get; set; }
        public string adalek { get; set; }
        public string RollMaxSize { get; set; }
        public string AsprovaRollMaxSize { get; set; }
        public string SalesOrder { get; set; }

        public ViewOrder(string code, string connectingorder, string itemcode, string itemname, 
            string porkeverek, string foliatype, string foliatypecatalog, string color, double qty, string jumboqty, string jumbolength, string jumboweight,
            string jumbosize, string customer, DateTime starttime, DateTime endtime, string machine, DateTime orderLET, string asRollMax, string actSalesOrder )
        {
            Code = code;
            ConnectingOrder = connectingorder;
            ItemCode = itemcode;
            ItemName = itemname;
            Porkeverek = porkeverek;
            FoliaType = foliatype;
            FoliaTypeCatalog = foliatypecatalog;
            Color = color;
            Qty = qty;
            JumboQty = jumboqty;
            JumboLength = jumbolength;
            JumboWeight = jumboweight;
            JumboSize = jumbosize;
            Customer = customer;
            StartTime = starttime;
            EndTime = endtime;
            Machine = machine;
            OrderDueDate = orderLET;
            AsprovaRollMaxSize = asRollMax;
            SalesOrder = actSalesOrder;
        }

    }
}
