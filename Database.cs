﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace AsprovaDB
{
    class Database
    {
        static string connectionString = "datasource=localhost;port=3306;username=root;password=password;CharSet=utf8;";

        public static Dictionary<string, string> GetWHOCodesAndDescriptions()
        {
            return TwoFieldDatabaseQuery("SELECT WHO, CONCAT_WS(' - ', WHO, WHO_description) AS WHO_WHODESC FROM operation_scheduling.who;");
        }

        public static Dictionary<string, string> GetTisztasagiFok()
        {
            return TwoFieldDatabaseQuery("SELECT * FROM operation_scheduling.spec1;");
        }

        public static Dictionary<string, string> GetMuto(string tisztasagiFok)
        {
            if (tisztasagiFok == "1" || tisztasagiFok == "2")
                return TwoFieldDatabaseQuery("SELECT * FROM operation_scheduling.resource where Eroforras_csoport = 'Aseptikus műtő';");
            else
                return TwoFieldDatabaseQuery("SELECT * FROM operation_scheduling.resource where Eroforras_csoport = 'Septikus műtő';");
        }

        static Dictionary<string, string> TwoFieldDatabaseQuery(string query)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            MySqlConnection sqlConnection = new MySqlConnection(connectionString);
            try
            {
                using (MySqlCommand command = new MySqlCommand(query, sqlConnection))
                {
                    sqlConnection.Open();
                    MySqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(reader[0].ToString(), reader[1].ToString());
                    }
                }
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
            return result;
        }

        static System.Data.DataTable AllFieldDatabaseQuery(string query)
        {
            System.Data.DataTable DT = new System.Data.DataTable();

            MySqlConnection sqlConnection = new MySqlConnection(connectionString);

            try
            {
                using (MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(query, sqlConnection))
                {
                    sqlConnection.Open();
                    mySqlDataAdapter.Fill(DT);
                }
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
            return DT;
        }

        public static void UpdateIntegratedMasterTable(string who, string folyamatSzam, string folyamatKod, string utasitas_tipus, string utasitas_kod, string eroforras_tetel, string termeles)
        {
            string query = "INSERT INTO `operation_scheduling`.`integrated_master` " +
                "(`Tetel_WHO`, `Folyamat_szam`, `Folyamat_kod`, `Utasitas_tipus`, `Utasitas_kod`, `Eroforras_tetel`, `Termeles`) " +
                "VALUES ('" + who + "', '" + folyamatSzam + "', '" + folyamatKod + "', '" + utasitas_tipus + "', '" + utasitas_kod + "', '" + eroforras_tetel + "', '" + termeles + "') " +
                "ON DUPLICATE KEY UPDATE " +
                "Tetel_WHO = VALUES(Tetel_WHO), " +
                "Folyamat_szam = VALUES(Folyamat_szam), " +
                "Folyamat_kod = VALUES(Folyamat_kod), " +
                "Utasitas_tipus = VALUES(Utasitas_tipus), " +
                "Utasitas_kod = VALUES(Utasitas_kod), " +
                "Eroforras_tetel = VALUES(Eroforras_tetel), " +
                "Termeles = VALUES(Termeles) " +
                ";";

            ExecuteCommand(query);
        }

        public static void UpdateOrderTable(string mutetAzon, string who, string tisztasagiFok, string mutetSurgosseg, string mutetIdeje, string rendelesiMennyiseg, string prioritas)
        {
            string query = "INSERT INTO `operation_scheduling`.`order_tbl` " +
                "(`Mutet azonosito`, `WHO_kod`, `Tisztasagi_fok`, `Mutet_surgossege`, `Mutet_ideje`, `Rendelesi_mennyiseg`, `Prioritas`) " +
                "VALUES('" + mutetAzon + "', '" + who + "', '" + tisztasagiFok + "', '" + mutetSurgosseg + "', '" + mutetIdeje + "', '" + rendelesiMennyiseg + "', '" + prioritas + "');";

            ExecuteCommand(query);
        }

        public static void AddOrvos(int o_kod, string nev, string szak)
        {
            MySqlConnection myconnection = new MySqlConnection(connectionString);
            try
            {
                String query = "INSERT INTO Orvos VALUES(@o_kod, @nev, @szak, 0)";
                using (MySqlCommand command = new MySqlCommand(query, myconnection))
                {
                    myconnection.Open();
                    command.Parameters.AddWithValue("@o_kod", o_kod);
                    command.Parameters.AddWithValue("@nev", nev);
                    command.Parameters.AddWithValue("@szak", szak);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                myconnection.Close();
            }
        }

        static void ExecuteCommand(string query)
        {
            MySqlConnection sqlConnection = new MySqlConnection(connectionString);
            try
            {
                using (MySqlCommand command = new MySqlCommand(query, sqlConnection))
                {
                    sqlConnection.Open();
                    command.ExecuteReader();
                }
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}
